using Projectiles;
using UnityEngine;

namespace Player
{
    public class ArmController : MonoBehaviour
    {
        [SerializeField]
        private ProjectileSpawner _ProjectileSpawner;
        
        [SerializeField]
        private Transform _ArmEnd;
        
        [SerializeField]
        private float _FirePower = 3f;

        public void Aim(Vector3 direction)
        {
            var directionToLookAt = (direction - transform.position).normalized;
            directionToLookAt.y = Mathf.Clamp(directionToLookAt.y, 0, 180);
            var rotatedVector = Quaternion.AngleAxis(transform.parent.eulerAngles.z, Vector3.forward) * directionToLookAt;
            transform.right = rotatedVector;
        }

        public void Shoot(int playerID)
        {
            _ProjectileSpawner.SpawnProjectile(_ArmEnd.position, transform.rotation, transform.right.normalized * _FirePower, playerID);
        }
    }
}