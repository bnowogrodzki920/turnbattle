using System;
using Elympics;
using UnityEngine;

namespace Player
{
    public class PlayerHealthController : ElympicsMonoBehaviour, IInitializable
    {
        [SerializeField]
        private int _MaxHealth = 100;

        private ElympicsInt _CurrentDamage;
        private ElympicsInt _CurrentHealth;

        public int MaxHealth => _MaxHealth;
        public int CurrentHealth => _CurrentHealth.Value;

        public event Action<int> OnHealthValueChanged;
        private void InvokeOnHealthValueChanged(int lastValue, int newValue) => OnHealthValueChanged?.Invoke(newValue);
        
        public void Initialize()
        {
            _CurrentHealth = new ElympicsInt(_MaxHealth);
            _CurrentDamage = new ElympicsInt();
            _CurrentHealth.ValueChanged += InvokeOnHealthValueChanged;
        }

        public void Damage(int damage)
        {
            if (!Elympics.IsServer)
            {
                return;
            }
            
            if (damage > _CurrentHealth.Value)
            {
                damage = _CurrentHealth.Value;
            }

            _CurrentHealth.Value -= damage;
            
            if (_CurrentDamage.Value != 0)
            {
                Debug.Log($"[{nameof(PlayerHealthController)}.{nameof(Damage)}] Player {PredictableFor} health was decreased! Current Health: {_CurrentHealth.Value}", this);
                _CurrentDamage.Value = 0;
            }
        }

        private void OnDestroy()
        {
            _CurrentHealth.ValueChanged -= InvokeOnHealthValueChanged;
        }
    }
}