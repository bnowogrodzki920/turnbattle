using System;
using Elympics;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerController : ElympicsMonoBehaviour, IInputHandler, IUpdatable
    {
        [SerializeField]
        private PlayerInputProvider _PlayerInputProvider;
        
        [SerializeField]
        private PlayerHealthController _PlayerHealthController;

        [SerializeField]
        private Rigidbody2D _Rigidbody;

        [SerializeField]
        private ArmController _Arm;

        [SerializeField]
        private int _PlayerID;

        public event Action OnHealthChanged;
        private void InvokeOnHealthChanged(int newValue) => OnHealthChanged?.Invoke();

        public int MaxHealth => _PlayerHealthController.MaxHealth;
        public int CurrentHealth => _PlayerHealthController.CurrentHealth;

        private void Awake()
        {
            _PlayerHealthController.OnHealthValueChanged += InvokeOnHealthChanged;
        }

        public void OnInputForClient(IInputWriter inputSerializer)
        {
            if (Elympics.Player == ElympicsPlayer.FromIndex(_PlayerID))
            {
                SerializeInput(inputSerializer);
            }
        }

        public void OnInputForBot(IInputWriter inputSerializer)
        {
            //SerializeInput(inputSerializer);
        }

        public void ElympicsUpdate()
        {
            if (ElympicsBehaviour.TryGetInput(PredictableFor, out var inputReader))
            {
                var moveMagnitude = Vector2.zero;
                var aimVector = Vector2.zero;
                
                inputReader.Read(out moveMagnitude.x);
                inputReader.Read(out aimVector.x);
                inputReader.Read(out aimVector.y);
                inputReader.Read(out bool fired);
                
                Move(moveMagnitude);
                Aim(aimVector);

                if (fired)
                {
                    _Arm.Shoot((int)PredictableFor);
                }
            }
        }

        private void Move(Vector2 moveMagnitude)
        {
            _Rigidbody.velocity = moveMagnitude;
        }

        private void Aim(Vector2 aimVector)
        {
            _Arm.Aim(aimVector);
        }

        private void SerializeInput(IInputWriter inputSerializer)
        {
            inputSerializer.Write(_PlayerInputProvider.MoveMagnitude.x);
            inputSerializer.Write(_PlayerInputProvider.AimVector.x);
            inputSerializer.Write(_PlayerInputProvider.AimVector.y);
            inputSerializer.Write(_PlayerInputProvider.Fired);
        }

        public void OnPlayerHit(int damage)
        {
            _PlayerHealthController.Damage(damage);
        }

        private void OnDestroy()
        {
            _PlayerHealthController.OnHealthValueChanged += InvokeOnHealthChanged;
        }
    }
}