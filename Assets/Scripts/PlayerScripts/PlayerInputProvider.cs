using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerInputProvider : MonoBehaviour
    {
        [SerializeField]
        private PlayerInput _PlayerInput;

        [SerializeField]
        private float _MovementSpeed = 1f;

        public Vector2 MoveMagnitude { get; private set; }
        public Vector2 AimVector { get; private set; }
        public bool Fired { get; private set; }

        private InputAction _MoveAction;
        private InputAction _AimAction;

        private bool _UpdateAim;
        
        private void Awake()
        {
            _PlayerInput.actions.FindAction("Fire").performed += OnFire;
            _MoveAction = _PlayerInput.actions.FindAction("Move");
            _AimAction = _PlayerInput.actions.FindAction("Aim");
        }

        private void Update()
        {
            MoveMagnitude = _MoveAction.ReadValue<Vector2>() * _MovementSpeed;

            if (_UpdateAim)
            {
                var mousePosition = _AimAction.ReadValue<Vector2>();
                AimVector = Camera.main.ScreenToWorldPoint(mousePosition);
            }
        }

        private void OnFire(InputAction.CallbackContext context)
        {
            _UpdateAim = !_UpdateAim;

            if (_UpdateAim)
            {
                return;
            }

            Fired = true;
            StartCoroutine(DisableFiredFlagOnFixedUpdate());
        }

        private IEnumerator DisableFiredFlagOnFixedUpdate()
        {
            yield return new WaitForFixedUpdate();
            Fired = false;
        }

        private void OnDestroy()
        {
            //_PlayerInput.actions.FindAction("Fire").performed -= OnFire;
        }
    }
}
