using System;
using UnityEngine;
using UnityEngine.UI;
using Wind;

namespace UI
{
    public class WindIndicator : MonoBehaviour
    {
        [SerializeField]
        private WindController _WindController;
        
        [SerializeField]
        private Image _LeftSide;
        
        [SerializeField]
        private Image _RightSide;

        [SerializeField]
        private Color[] _ColorsForGradient;

        private Gradient _Gradient;
        
        private void Awake()
        {
            _WindController.WindForce.ValueChanged += UpdateWindIndicator;

            _Gradient = new Gradient();
            var gradientColorKeys = new GradientColorKey[_ColorsForGradient.Length];
            var gradientAlphaKeys = new GradientAlphaKey[_ColorsForGradient.Length];
            
            for (var i = 0; i < _ColorsForGradient.Length; i++)
            {
                gradientColorKeys[i] = new GradientColorKey(_ColorsForGradient[i], (float)i / _ColorsForGradient.Length);
                gradientAlphaKeys[i] = new GradientAlphaKey(_ColorsForGradient[i].a, (float)i / _ColorsForGradient.Length);
            }
            
            _Gradient.SetKeys(gradientColorKeys, gradientAlphaKeys);

            UpdateWindIndicator(1, 1);
        }

        private void UpdateWindIndicator(float lastValue, float wind)
        {
            var windPercentage = Mathf.Abs(wind / (wind < 0 ? _WindController.MinWind : _WindController.MaxWind));

            switch (wind)
            {
                case < 0:
                    _LeftSide.fillAmount = windPercentage;
                    _LeftSide.color = _Gradient.Evaluate(windPercentage);
                    _RightSide.fillAmount = 0;
                    break;
                case > 0:
                    var windPercent = Mathf.Abs(wind / _WindController.MinWind);
                    _LeftSide.fillAmount = 0;
                    _RightSide.fillAmount = wind / _WindController.MaxWind;
                    _RightSide.color = _Gradient.Evaluate(windPercentage);
                    break;
                default:
                    _LeftSide.fillAmount = 0;
                    _RightSide.fillAmount = 0;
                    break;
            }
        }
    }
}