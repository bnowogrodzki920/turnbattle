using Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField]
        private PlayerController _PlayerController;

        [SerializeField]
        private Image _Image;
        
        private void Awake()
        {
            _PlayerController.OnHealthChanged += UpdateHealthBar;
            _Image.fillAmount = 1;
        }

        private void UpdateHealthBar()
        {
            var healthPercentage = (float)_PlayerController.CurrentHealth / _PlayerController.MaxHealth;
            _Image.fillAmount = healthPercentage;
        }
    }
}
