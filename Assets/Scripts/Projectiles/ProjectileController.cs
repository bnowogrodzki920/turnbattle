using System;
using System.Collections;
using Elympics;
using Player;
using UnityEngine;
using Wind;

namespace Projectiles
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class ProjectileController : ElympicsMonoBehaviour, IUpdatable
    {
        [SerializeField]
        private Rigidbody2D _Rigidbody;
        
        [SerializeField]
        private Collider2D _Collider;

        [SerializeField]
        private float _LifetimeInSeconds = 5;

        [SerializeField]
        private int _Damage = 10;

        private WindController _WindController;
        
        private ElympicsBool _MarkAsDestroyed = new();
        private ElympicsFloat _CurrentLifetime = new(0);

        private ElympicsInt _OwnerID = new ElympicsInt(-1);

        public void SetOwnerID(int id)
        {
            _OwnerID.Value = id;
        }
        
        public void Shoot(Vector2 force)
        {
            _Rigidbody.AddForce(force, ForceMode2D.Impulse);
        }

        public void UpdateVelocity(float force)
        {
            var currentVelocity = _Rigidbody.velocity;
            currentVelocity.x += force;
            _Rigidbody.velocity = currentVelocity;
        }
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject == gameObject) // rare case when projectile has two colliders (i.e. composite is not working properly)
            {
                return;
            }

            if (other.gameObject.CompareTag("Projectile"))
            {
                return;
            }

            if (other.gameObject.CompareTag("Player"))
            {
                other.gameObject.GetComponent<PlayerController>().OnPlayerHit(_Damage);
            }

            _Rigidbody.isKinematic = false;
            _Collider.enabled = false;
            _MarkAsDestroyed.Value = true;
        }

        public void ElympicsUpdate()
        {
            if (!Elympics.IsClientOrBot)
            {
                return;
            }

            _CurrentLifetime.Value += Elympics.TickDuration;

            if (_CurrentLifetime.Value > _LifetimeInSeconds || _MarkAsDestroyed.Value)
            {
                ElympicsDestroy(gameObject);
            }
        }
    }
}