using System.Collections.Generic;
using Elympics;
using UnityEngine;
using Wind;

namespace Projectiles
{
    public class ProjectileSpawner : ElympicsMonoBehaviour
    {
        [SerializeField]
        private string _PathToProjectile;
        
        [SerializeField]
        private WindController _WindController;

        private List<ProjectileController> _Projectiles = new();

        public void SpawnProjectile(Vector2 position, Quaternion rotation, Vector2 force, int playerIndex)
        {
            var projectileGO = ElympicsInstantiate(_PathToProjectile, ElympicsPlayer.FromIndex(playerIndex));
            var projectile = projectileGO.GetComponent<ProjectileController>();
            projectile.transform.SetPositionAndRotation(position, rotation);
            projectile.transform.parent = transform;
            projectile.Shoot(force);
            projectile.UpdateVelocity(_WindController.WindForce.Value);
            projectile.SetOwnerID(playerIndex);
            
            _Projectiles.Add(projectile);
        }
    }
}