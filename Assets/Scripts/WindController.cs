using System;
using Elympics;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Wind
{
    public class WindController : ElympicsMonoBehaviour, IInitializable
    {
        [SerializeField]
        private Vector2 _MinMaxWind = new(-1, 1);

        public float MinWind => _MinMaxWind.x;
        public float MaxWind => _MinMaxWind.y;
        
        public readonly ElympicsFloat WindForce = new();
        
        public void Initialize()
        {
            ChangeWind();
        }
        
        public void ChangeWind()
        {
            if (!Elympics.IsServer)
            {
                return;
            }
            
            WindForce.Value = Random.Range(_MinMaxWind.x, _MinMaxWind.y);
            Debug.Log($"[{nameof(WindController)}.{nameof(ChangeWind)}] Changed wind to {WindForce.Value}", this);
        }
    }
}
